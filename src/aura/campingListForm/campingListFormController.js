({
	clickCreateItem : function(component, event, helper) {
		/* If the form is valid, the JavaScript controller pushes the newItem onto the array of existing items, 
			triggers the notification that the items value provider has changed, 
			and resets the newItem value provider with a blank sObjectType of Camping_Item__c.*/

		var validItem = component.find('itemform').reduce(function (validSoFar, inputCmp) {
			// Displays error messages for invalid fields
			inputCmp.showHelpMessageIfInvalid();

			return validSoFar && inputCmp.get('v.validity').valid;
		}, true);

		// If we pass error checking, do some real work
		if (validItem) {
			// Create the new item
			var newItem = component.get("v.newItem");
			console.log("Create item: " + JSON.stringify(newItem));
			helper.createItem(component, newItem);
		}
	},
})