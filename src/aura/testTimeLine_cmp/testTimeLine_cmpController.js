({
	clickHandleCollapse : function(component, event, helper) {
		//colapsa o expande el Div collapsable
		var divCollapsable = component.find('divCollapsable');
		//console.log(divCollapsable);
		$A.util.toggleClass(divCollapsable, 'slds-is-open')
	}
})