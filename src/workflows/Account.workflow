<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Nueva_cuenta_activa</fullName>
        <description>Nueva cuenta activa</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Nueva_cuenta</template>
    </alerts>
    <fieldUpdates>
        <fullName>Actualiza_valoracion</fullName>
        <field>Rating</field>
        <literalValue>Hot</literalValue>
        <name>Actualiza valoración</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Enviar correo</fullName>
        <actions>
            <name>Nueva_cuenta_activa</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Actualiza_valoracion</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Cuenta_Nueva</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Active__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Cuenta_Nueva</name>
                <type>Task</type>
            </actions>
            <timeLength>-5</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Cuenta_Nueva</name>
                <type>Task</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>Cuenta_Nueva</fullName>
        <assignedTo>ricardo@pereira.dev</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Ha sido creada una cuenta nueva</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Cuenta Nueva</subject>
    </tasks>
</Workflow>
