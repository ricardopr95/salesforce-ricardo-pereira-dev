public with sharing class CPasajeros {
	/**
	*este metodo devuelve los Pasajeros cuyos NIF han sido validados correctamente
	*/
	public static List<Pasajero__c> valNif(List<Pasajero__c> newSet){
		List<Pasajero__c>dniCorrecto=new List<Pasajero__c>();
		
		for(Pasajero__c pas:newSet){
			if(FormatDNI.validate(pas.nif__c)){
				dniCorrecto.add(pas);
			}else{//si el NIF no tiene formato correcto, no lo deja pasar
				pas.NIF__c.adderror(Label.Error_Nif);
			}
		}
		return dniCorrecto;
	}
	
	public static void localizarContacto(List<Pasajero__c> newSet){
		//Preparo dos set que contengan los NIF de los contactos y los pasajeros 
		//que serán modificados
		Set<String> setNifContactos=new Set<String>();
		Set<Pasajero__c> setPasSinCont=new Set<Pasajero__c>();
	
		//El siguiente bucle puebla los Sets anteriores
		for(Pasajero__c pas:newSet){
			
				if(pas.Contacto__c==null){
					setNifContactos.add(pas.NIF__c);
					setPasSinCont.add(pas);
				}
			
		}
	
		Contact[] c=[Select Id, NIF__c from Contact where NIF__c in:setNifContactos];
	
		//si encuentro un Contaccto con el mismo NIF se lo adjudico al Pasajero
		for(Pasajero__c pas:setPasSinCont){
			for(Contact cont:c){
				if(pas.NIF__c.equalsignorecase(cont.NIF__c)){
					pas.Contacto__c=cont.id;
					break;
				}
			}
		}
	}
}