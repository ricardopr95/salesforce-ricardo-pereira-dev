public with sharing class c_SustituirUsuario {

    private List<SelectOption> opciones=new List<SelectOption>();
    public Account acc1{get;set;}//Para el usuario A introducir
    public Account acc2{get;set;}//Par el usuario a sustituir

    public String opcion{get;set;}
    public SelectOption sel1=new SelectOption('ins','Insertar usuario');
    public SelectOption sel2=new SelectOption('eli','Eliminar usuario');

    
    public c_SustituirUsuario() {
        opciones.add(sel1);
        opciones.add(sel2);

        acc1=new Account();
        acc2=new Account();
    }

   /* public String getRecordName() {
        //return 'Hello ' + (String)mysObject.get('name') + ' (' + (Id)mysObject.get('Id') + ')';
    }*/

    public void mostrar(){
        opciones.get(0).setLabel('vaia');
        String mensaje='Esto funciona';
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, mensaje));
    }

    public List<SelectOption> getOpciones(){
        return opciones;
    }

    public User[] getName(){
        return [select Name from User];
    }
    
    //public Account getUser(){
      //  return [SELECT OwnerId FROM Account limit 1];
    //}

    public void save(){
    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, opcion));
        //if(sel1.getValue()){

        //}
        if(opcion==null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Ha de seleccionar una opción.'));
            return;
        }

        if(acc1.OwnerId==null||acc2.OwnerId==null){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Ha de seleccionar los usuarios.'));
             return;
        }

        if(opcion=='ins'){
            /*Debe incluirse el usuario seleccionado en el campo 
            “Usuario insertar/Eliminar” en los registros de 
            los objetos indicados en el custom setting Objetos_visibilidad__c,
             de los que el usuario indicado en el campo “Usuario sustituir”
             es propietario con permisos de Lectura/ Escritura
            
             */
             /*
             Objetos_visibilidad__c[] objetosAModificar=
             [SELECT OwnerId FROM Objetos_visibilidad__c WHERE Owner id=:acc2.OwnerId];

                for(Objetos_visibilidad__c obj:objetosAModificar ){
                    obj.OwnerId=acc1.OwnerId;
                }

                insert objetosAModificar;
             */
            return;
        }

        if(opcion=='eli'){
            /*
                Si se ha elegido la opción Eliminar usuario,
                 debe eliminarse de los objetos de Share 
                 indicados en Objetos_visibilidad__c.Objeto_visibilidad__c 
                 el usuario indicado en el campo Usuario insertar/Eliminar
            */


        }


    }

}