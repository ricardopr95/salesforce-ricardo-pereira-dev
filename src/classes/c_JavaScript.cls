global class c_JavaScript {

  	/*
  	*El  siguiente método valida si se puede crear un pedido a partir de la oportunidad
  	*
  	*Devuelve:
  	*  null- Si cumple las condiciones
  	*  mensaje- Cadena de texto que indica el porque no cumple las condiciones
  	*/
 	 webService static String comprobarGanada(Id id) {
  	  String mensaje=null;
 	   List<Opportunity> lst_opp=[select Id,StageName, AccountId from Opportunity where Id = :id limit 1];
 	   if(!lst_opp.isEmpty())
 	   	if(lst_opp[0].AccountId==null)return 'Es necesaria una cuenta para generar el pedido.';
 	     mensaje=(lst_opp[0].StageName!='Closed Won')?'El estado de la oportunidad no es Cerrada/Ganada':null;
	    return mensaje;
	  }
  
	
	/*
	*Crea un pedido a partir de la oportunidad y devuelve la Id del pedido
	*
	*/
	 webService static String opAPedido(Id id) {
	 	//Creo el pedido y obtengo la oportunidad mediante el Id
	 	Order pedido=new Order();
	 	List<Opportunity> ops=[select Id,AccountId, CloseDate, Pricebook2Id from Opportunity where Id = :id limit 1];
	 	
	 	//Vuelco los datos en el pedido
	 	pedido.AccountID=ops[0].AccountId;
		pedido.EffectiveDate=ops[0].CloseDate;
	 	pedido.Description='Generado corectamente.';
	 	pedido.status='Draft';
	 	
	 	//Adjudico la cuenta de la oportunidad en el pedido,  y le copio los datos de esta.
	 	 Account[] cnts=[select Id,BillingCity,BillingCountry, BillingLatitude,
	 	  BillingLongitude, BillingPostalCode, BillingState, BillingStreet,
	 	  ShippingCity,ShippingCountry, ShippingLatitude,
	 	  ShippingLongitude, ShippingPostalCode, ShippingState, ShippingStreet
	 	  from Account where Id = :ops[0].AccountId limit 1];
	 	 
	 	 pedido.BillingCity=cnts[0].BillingCity;
	 	 pedido.BillingCountry =cnts[0].BillingCountry;
	 	 pedido.BillingLatitude =cnts[0].BillingLatitude;
	 	 pedido.BillingLongitude =cnts[0].BillingLongitude;
	 	 pedido.BillingPostalCode =cnts[0].BillingPostalCode;
	 	 pedido.BillingState =cnts[0].BillingState;
	 	 pedido.BillingStreet =cnts[0].BillingStreet;
	 	 
	 	 pedido.ShippingCity=cnts[0].ShippingCity;
	 	 pedido.ShippingCountry =cnts[0].ShippingCountry;
	 	 pedido.ShippingLatitude =cnts[0].ShippingLatitude;
	 	 pedido.ShippingLongitude =cnts[0].ShippingLongitude;
	 	 pedido.ShippingPostalCode =cnts[0].ShippingPostalCode;
	 	 pedido.ShippingState =cnts[0].ShippingState;
	 	 pedido.ShippingStreet =cnts[0].ShippingStreet;
	 	 
	 	 pedido.Pricebook2Id=ops[0].Pricebook2Id;
	 	  
	 	 insert(pedido);
	 	 
	 	 //Obtengo los productos de la oportunnidad y los relaciono con el pedido
	 	 OpportunityLineItem[] lines=[SELECT Id, PricebookEntryId, Quantity,UnitPrice FROM OpportunityLineItem WHERE OpportunityId=:ops[0].Id];
	 	 List<OrderItem> otList=new List<OrderItem>();
	 	 
	 	
	 	 
	 	 OrderItem orderIt;
	 	 
	 	 for(OpportunityLineItem olt:lines){
	 	 	orderIt=new OrderItem();
	 	 	orderIt.PricebookEntryId=olt.PricebookEntryId;
	 	 	orderIt.Quantity=olt.Quantity;
	 	 	orderIt.UnitPrice=olt.UnitPrice;
	 	 	orderIt.OrderId=pedido.Id;
	 	 	otlist.add(orderIt);
	 	 }
	 	 
	 	upsert(otList);
	 	
	 	return pedido.id;
  	  	
	 }
	 
	 
	 
	 /*
	  * Crea pedidos a partir de las oportunidad 
	  */
	 public static void opAPedido(Map<Id, Opportunity> mapOp) {
	 	//Creo el pedido y obtengo la oportunidad mediante el Id
	 	Order pedido;
	 	Set<Order>setPedidos=new Set<Order>();
	 	List<Opportunity> ops=[select Id,AccountId, CloseDate, Pricebook2Id,
	 	Account.BillingCity,Account.BillingCountry, Account.BillingLatitude,
	 	Account.BillingLongitude, Account.BillingPostalCode, Account.BillingState, Account.BillingStreet,
	 	Account.ShippingCity, Account.ShippingCountry, Account.ShippingLatitude,
	 	Account.ShippingLongitude, Account.ShippingPostalCode, Account.ShippingState, Account.ShippingStreet 
	 	from Opportunity where Id IN:mapOp.values()];
	 	
	 	//este mapa relaciona la id de la oportunidad con la id del pedido
	 	Map<Id,Id> mapOpPedido=new Map<Id,Id>();
	 	
	 	//Obtengo los productos de la oportunnidad 
	 	 OpportunityLineItem[] lines=[SELECT Id, PricebookEntryId, Quantity,UnitPrice,OpportunityId FROM OpportunityLineItem WHERE OpportunityId IN:mapOp.values()];
	 	 List<OrderItem> otList=new List<OrderItem>();
	 	 OrderItem orderIt;
	 	 
	 	
	 	for(Opportunity op:ops){
	 		pedido=new Order();
	 		
	 		//Vuelco los datos en el pedido
	 		pedido.AccountID=op.AccountId;
			pedido.EffectiveDate=op.CloseDate;
	 		pedido.Description='Generado corectamente.';
	 		pedido.status='Draft';
	 		
	 		//Adjudico la cuenta de la oportunidad en el pedido,  y le copio los datos de esta.
	 		pedido.BillingCity=op.Account.BillingCity;
	 	 	pedido.BillingCountry =op.Account.BillingCountry;
	 		pedido.BillingLatitude =op.Account.BillingLatitude;
	 		pedido.BillingLongitude =op.Account.BillingLongitude;
	 	 	pedido.BillingPostalCode =op.Account.BillingPostalCode;
	 	 	pedido.BillingState =op.Account.BillingState;
	 	 	pedido.BillingStreet =op.Account.BillingStreet;
	 	 
	 	 	pedido.ShippingCity=op.Account.ShippingCity;
	 		pedido.ShippingCountry =op.Account.ShippingCountry;
	 	 	pedido.ShippingLatitude =op.Account.ShippingLatitude;
	 	 	pedido.ShippingLongitude =op.Account.ShippingLongitude;
	 	 	pedido.ShippingPostalCode =op.Account.ShippingPostalCode;
	 	 	pedido.ShippingState =op.Account.ShippingState;
	 	 	pedido.ShippingStreet =op.Account.ShippingStreet;
	 	 	
	 	 	pedido.Pricebook2Id=ops[0].Pricebook2Id;
	 	 	insert(pedido);
	 	 	mapOpPedido.put(op.id,pedido.id);
	 	 	//setPedidos.add(pedido);
	 	 
	 	}
	 	
	 	//insert(new List<Order>(setPedidos));
	 	
	 	for(Opportunity op:ops){
	 		//relaciono los productos con sus respectivos pedidos
	 	 for(OpportunityLineItem olt:lines){
	 	 	if(olt.OpportunityId==op.Id){
	 	 	orderIt=new OrderItem();
	 	 	orderIt.PricebookEntryId=olt.PricebookEntryId;
	 	 	orderIt.Quantity=olt.Quantity;
	 	 	orderIt.UnitPrice=olt.UnitPrice;
	 	 	orderIt.OrderId=mapOpPedido.get(op.Id);
	 	 	otlist.add(orderIt);
	 	 	}
	 	 }
	 	}
	 	insert(otList);
	 	
	 }
}