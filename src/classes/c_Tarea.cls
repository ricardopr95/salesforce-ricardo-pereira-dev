public with sharing class c_Tarea {
	/*
	* Cuando una tarea es modificada y pasa a estar completa (Status= 'Completed'),
	* 
	* - Si está asociado a una cuenta, cambiar e campo Fecha_ultima_modificacion de la cuenta padre de esta
	*   por el valor de la fecha de fin de la tarea
	*			Tarea-->Cuenta-->Cuenta
	*
	* - Si está asociado a una oportunidad, cambiar el campo Fecha_ultima_modificacion de la cuenta padre de la cuenta
	*	a la que está asociada la oportunidad por el valor de la fecha de fin de la tarea
	*			Tarea-->Oportunidad-->Cuenta-->Cuenta
	*/
	public static void cambiarFechaAccount(List<Task> tnew, List<Task> told){
		
		/*
		 *El objetivo es rellenar un mapa de forma que, 
		 *las key sean las Id del WhatId de la tarea y
		 *las values sean las Id de las Cuentas a Modificar
		 *
		 *Y otro contenga las Id de las cuentas
		 *y las cuentas en si
		 */
		 Map<Id,Account>mapCuentasMod=new Map<Id,Account>();
		 Map<Id,Id>mapRelTarCuenta=new Map<Id,Id>();
		 
		//Primero llenamos un Mapa con las tareas que han sido modificadas a estado completado
		Map<Task, Id> mapTareas=new Map<Task,Id>();
		for(Integer i=0;i<tnew.size();i++){
			if(tnew[i].status!=told[i].status){
				if(tnew[i].Status=='Completed' && tnew[i].WhatId!=null){
					mapTareas.put(tnew[i],tnew[i].WhatId);
				}
			}
		}
		
		//Llenamos el mapa a modificar con las primeras cuentas
		//Tarea-->Cuenta-->Cuenta
		for(Account ac:[SELECT Id, ParentId FROM Account WHERE Id IN:mapTareas.values()]){
			mapRelTarCuenta.put(ac.Id,ac.ParentId);
		}
		
		//Llenamos el mapa con la siguiente tanda de cuentas
		//Tarea-->Oportunidad-->Cuenta-->Cuenta
		for(Opportunity op:[SELECT Id,Account.ParentId From Opportunity WHERE Id IN:mapTareas.values()]){
			mapRelTarCuenta.put(op.Id,op.Account.ParentId);
		}
		
		//Pueblo mapCuentasMod a partir de las Id de las cuentas a modificar
		mapCuentasMod.putAll([SELECT Id, Fecha_ultima_actividad_mas_reciente__c From Account WHERE Id IN:mapRelTarCuenta.values()]);
		
		//Recorro mapTareas, y comparo las fechas con las fechas a modificar,
		//si se cumplen las condiciones, modifico
		Date fecha;
		for(Task tar:mapTareas.keyset()){
			fecha=mapCuentasMod.get(mapRelTarCuenta.get(tar.WhatId)).Fecha_ultima_actividad_mas_reciente__c;
			if(fecha!=null){
				if(fecha<tar.ActivityDate){
				 mapCuentasMod.get(mapRelTarCuenta.get(tar.WhatId)).Fecha_ultima_actividad_mas_reciente__c=tar.ActivityDate;
				}
			}else{
				mapCuentasMod.get(mapRelTarCuenta.get(tar.WhatId)).Fecha_ultima_actividad_mas_reciente__c=tar.ActivityDate;
			}
		}
		update mapCuentasMod.values();
	}
}