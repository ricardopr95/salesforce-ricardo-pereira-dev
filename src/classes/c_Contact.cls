public with sharing class c_Contact {
	public c_Contact() {
	}

	public static void enviarCorreo(Contact[] conts){
		Map<Contact,String> mapContactos =new Map<Contact,String>();
		for(Contact cont:conts){
			if(cont.Enviar_email__c==true){
				if(cont.Email!=null){
					mapContactos.put(cont,cont.Email);
				}else{
				cont.Email.addError('El contacto debe disponer de un Email.');
				}
			}
		}
		if(mapContactos.size()>0){
			try{
				c_EnviarEmail.enviar(mapContactos.values(), 'Últimas ofertas!', null, null, 
					'ESTAS RECIBIENDO LAS OFERTAS MAS<BR/> <H1>LOOOOOOOOOCCCCAAAASSSS</H1> <BR/> DE LA TEMPORADAAAAAA',
					 true);
				for(Contact cont:mapContactos.keySet()){
					cont.Enviar_email__c=false;
				}
				}catch(HandledException he){

			}
		}
	}
}