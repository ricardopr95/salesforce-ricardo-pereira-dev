public with sharing class GestionProyectos {
	public Proyecto__c proyecto{get;set;}
	public transient List<Recurso__c> recursos{get;set;}
	public transient Map<Id, Empleado__c> personal{get;set;}
	public Map<Empleado__c, Map<Date, Recurso__c>> mapRecursos {get;set;}

	public List<Date> fechas{get;set;}

	public GestionProyectos() {
		mapRecursos=new Map<Empleado__c, Map<Date, Recurso__c>>();
		Id projectId= ApexPages.currentPage().getParameters().get('id');
		proyecto=[SELECT Id, Name FROM Proyecto__c WHERE Id=:projectId];
		recursos=[SELECT Id, Name, Jornada__c, Role__c, Read_Only_Role__c, Empleado__c FROM Recurso__c where Proyecto__c=:projectId];

		generarFechas(1, 20);

		Set<Id> setIdsEmpl=new Set<Id>();
		for(Recurso__c res: recursos){	
			setIdsEmpl.add(res.Empleado__c);
		}

		personal= new Map<Id, Empleado__c>([SELECT Id, Name FROM Empleado__c WHERE Id IN:setIdsEmpl]);

		for(Empleado__c emple: personal.values()){
			mapRecursos.put(emple, new Map<Date, Recurso__c>());
		}

		for(Recurso__c res:recursos){
			mapRecursos.get(personal.get(res.Empleado__c)).put(res.Jornada__c, res);
		}

		//Rellenar vacios para evitar error en la VF
		Recurso__c tempResVacio=new Recurso__c(Name='##VACIO##');
		for(Map<Date, Recurso__c> mapa:mapRecursos.values()){
			for(Date fecha:fechas){
				if(!mapa.containsKey(fecha)){
					mapa.put(fecha, tempResVacio);
				}
			}
		}

	}

	
	private void generarFechas(Integer diAn, Integer diSig){
		Integer diasAnteriores=diAn;
		Integer diasSiguientes=diSig;
		Date temp=System.Date.today();

		fechas = new List<Date>();
		fechas.add(temp);

		for(Integer i=1;i<=diasAnteriores;i++){
			fechas.add(temp.addDays(-i));
		}

		for(Integer i=1;i<=diasSiguientes;i++){
			fechas.add(temp.addDays(i));
		}

		fechas.sort();
	}
}