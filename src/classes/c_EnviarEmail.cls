public with sharing class c_EnviarEmail {
	public c_EnviarEmail() {
		
	}

	public static void enviar(String[] dirs, String asunto,
	 String[] cc, String[] cco, String cuerpo, Boolean firma){
		Messaging.reserveSingleEmailCapacity(dirs.size()+
			((cc!=null)?cc.size():0)+((cco!=null)?cco.size():0));
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

		// Assigna todas las direcciones a las que serán enviadas
		if(dirs!=null)mail.setToAddresses(dirs);
		if(cc!=null)mail.setCcAddresses(cc);
		if(cco!=null)mail.setbccAddresses(cco);

		mail.setReplyTo(UserInfo.getUserEmail());
		
		mail.setSenderDisplayName(UserInfo.getName());

		mail.setSubject(asunto);

		mail.setUseSignature(firma);

		mail.setPlainTextBody('Sentimos informarle de que no podrá visualizar correctamente este correo \nya que su servicio de Email no soporta HTML \n'+cuerpo);

		mail.setHtmlBody(cuerpo);

		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		

	}
}