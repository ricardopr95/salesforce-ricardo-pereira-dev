/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestTarjetaCredito {

    static testMethod void myUnitTest() {

		Account acc=new Account();
        acc.Name='Cuenta Prueba';
        acc.AnnualRevenue=20000;
        
        insert(acc);
        
        Contact con1=new Contact();
        con1.LastName='Contacto Prueba1';
        //con1.NIF__c='00000000A';
        con1.AccountId=acc.Id;
        
        //insert(con1);
        
        con1.NIF__c='00000069T';
        
        insert(con1);
        
        Tarjeta_credito__c tar=new Tarjeta_credito__c();
        tar.Ano_caducidad__c=2016;
        tar.Contacto__c=con1.Id;
        tar.Mes_caducidad__c=7;
        tar.Name='Mi tarjeta';
        tar.Num_Tarjeta__c='0987098709870987';
        tar.Tarjeta__c='Crédito';
        tar.Tipo_tarjeta__c='Visa oro';
        
        insert (tar);
        
        con1.Premium__c=true;
        update con1;
        
        tar=new Tarjeta_credito__c();
        tar.Ano_caducidad__c=2016;
        tar.Contacto__c=con1.Id;
        tar.Mes_caducidad__c=7;
        tar.Name='Mi tarjeta 2';
        tar.Num_Tarjeta__c='0987098709870987';
        tar.Tarjeta__c='Crédito';
        tar.Tipo_tarjeta__c='Visa oro';
        
        insert (tar);
        
    }
}