public with sharing class FormatDNI {
	public static Pattern pat=Pattern.compile('\\d{8}[TRWAGMYFPDXBNJZSQVHLCKEtrwagmyfpdxbnjzsqvhlcke]');
	public static String permitidosMayus='TRWAGMYFPDXBNJZSQVHLCKE';
	public static String permitidosMinus='trwagmyfpdxbnjzsqvhlcke';
	
	public static Boolean validate(String text){
		
		Matcher m=pat.matcher(text);
		if(m.matches()&&letraCorrecta(text)){
			return true;
		}else{
			return false;
		}
		
	}
	
	private static Boolean letraCorrecta(String text){
		String n=text.substring(0, 8);
		Integer num=Integer.valueOf(n);
		
		if(permitidosMayus.charAt(System.Math.mod(num, 23))==text.charat(8)||
		permitidosMinus.charAt(System.Math.mod(num, 23))==text.charat(8)){
		
		return true;
		
		}else{
			return false;
		}
		
	}
	
}