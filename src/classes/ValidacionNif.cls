public with sharing class ValidacionNif {

	public static void valContacto(List<Contact> newSet){
		for(Contact cont:newSet){
			if(!FormatDNI.validate(cont.nif__c)){
			
				cont.NIF__c.adderror(Label.Error_Nif);
			
			}
		}
	}
	
}