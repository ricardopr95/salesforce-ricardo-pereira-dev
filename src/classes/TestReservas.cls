/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestReservas {

    static testMethod void myUnitTest() {
        
        Account acc=new Account();
        acc.Name='Cuenta Prueba';
        acc.AnnualRevenue=20000;
        
        insert(acc);
        
        Contact con1=new Contact();
        con1.LastName='Contacto Prueba1';
        //con1.NIF__c='00000000A';
        con1.AccountId=acc.Id;
        
        //insert(con1);
        
        con1.NIF__c='00000069T';
        
        insert(con1);
        
        ReservaVuelo__c res=new ReservaVuelo__c();
        res.Clase__c='Turista';
        res.Contacto__c=con1.id;
        //res.Cuenta__c=acc.id;
        res.Destino__c='Bratislava';
        res.Fecha_Salida__c=System.now()+7;
        res.Origen__c='Madrid';
        res.Observaciones__c='asdfasdfasdf';
        
        insert(res);
        
        res.Origen__c='Bruselas';
        
        update(res);
        
        Pasajero__c pas1=new Pasajero__c();
        pas1.Edad__c=19;
        //pas1.NIF__c='00000000A';
        pas1.Nombre_Pasajero__c='Ricardo';
        pas1.Reserva__c=res.id;
        
        //insert(pas1);
        
        pas1.NIF__c='00000069T';
        
        insert(pas1);
        
    }
}