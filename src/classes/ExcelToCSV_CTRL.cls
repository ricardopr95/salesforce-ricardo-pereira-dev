public with sharing class ExcelToCSV_CTRL {

    public Blob xlsFile{get;set;}
    public Blob csvFile{get;set;}

    public ExcelToCSV_CTRL() {
        
    }

 
    // Pass in the endpoint to be used using the string url
    public void getCalloutResponseContents() {
    	System.debug('##xlsFile.toString(): '+xlsFile.toString());
        String url='https://api.cloudconvert.com/convert?apikey=3i8HzRpH7QZ0MeMq5dOoRmzeoApdfeRBwGsUKBoZKr65zR2vxvYxg-0r0hoPCSEYsJxFkn6_QqYinjtCqJP-mw&input=upload&download=inline&inputformat=csv&outputformat=html';
        // Instantiate a new http object
        Http h = new Http();
         // Instantiate a new HTTP request, specify the method (POST) as well as the endpoint
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'multipart/form-data');
        req.setBodyAsBlob(xlsFile);
        /*req.setHeader('apikey', '3i8HzRpH7QZ0MeMq5dOoRmzeoApdfeRBwGsUKBoZKr65zR2vxvYxg-0r0hoPCSEYsJxFkn6_QqYinjtCqJP-mw');
        req.setHeader('input', 'upload');
        req.setHeader('download', 'inline');
        req.setHeader('inputformat', 'csv');
        req.setHeader('outputformat', 'html');*/

        // Send the request, and return a response
        HttpResponse res = h.send(req);
        csvFile=res.getBodyAsBlob();

        system.debug('##csvFile.toString(): '+csvFile.toString());

    }
}