trigger Reserva_Account on ReservaVuelo__c (before insert) {

	Set<Id> idContactos=new Set<Id>();
	//Para hacer la query primero lleno un set on todos los ID de contactos
	//de las Reservas
	for(ReservaVuelo__c res:trigger.new){
		idContactos.add(res.Contacto__c);
	}
	
	Contact[] c=[Select Id, AccountId from Contact where Id in:idContactos];
	
	//Recorro las nuevas reservas y compruebo que la cuenta es nula y que el contacto
	//está en la lista, de ser asi, igualo la cuenta de la reserva a la cuenta del Contacto
	//Integer contador=0;
	Boolean found=false;
	for(ReservaVuelo__c res:trigger.new){
		if(res.Cuenta__c==null){
			for(Contact cont:c){
				if(res.Contacto__c==cont.Id){
					res.Cuenta__c=cont.AccountId;
					break;
				}
			}
			/*found=false;
			while(!found && contador<c.size()){
				if(res.Contacto__c==c[contador].Id){
					res.Cuenta__c=c[contador].AccountId;
					found=true;
				}
				contador++;
			}*/
		}
	}
}