trigger t_Cuentas on Account (after update) {
	/*
	*Para el caso en el que se ha actualizado el campo Fecha última actividad más reciente (Fecha_ultima_actividad_mas_reciente),
	*la cual no tiene rellenado el campo Cuenta Principal (es decir, es padre de otras cuentas),
	*ese dato deberá volcarse al campo Fecha última actividad más reciente (Fecha_ltima_actividad_m_s_reciente) de las cuenta hijas.
	 */
	Account[] cuentas=[SELECT Id,ParentId ,Fecha_ultima_actividad_mas_reciente__c FROM Account WHERE ParentId IN:trigger.newMap.keySet()];
	//recorro la lista de hijos: Si la fecha de estos es menor que la de del padre, la cambio
	for(Account c:cuentas){
		if(trigger.newMap.get(c.ParentId).Fecha_ultima_actividad_mas_reciente__c>c.Fecha_ultima_actividad_mas_reciente__c){
		c.Fecha_ultima_actividad_mas_reciente__c=trigger.newMap.get(c.ParentId).Fecha_ultima_actividad_mas_reciente__c;
		}
	}
	
	update cuentas;
}