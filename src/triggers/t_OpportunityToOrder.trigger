trigger t_OpportunityToOrder on Opportunity (after update) {
	
	//pueblo el set de ids a cambiar
	Map<Id, Opportunity> ids=new Map<Id, Opportunity>();
	
	for(Integer i=0;i<trigger.new.size();i++){
		if(trigger.new[i].AccountId!=null && trigger.new[i].convertir_pedido__c	&&
		 trigger.new[i].StageName=='Closed Won' &&
		 (trigger.new[i].convertir_pedido__c != trigger.old[i].convertir_pedido__c)){
			ids.put(trigger.new[i].id,trigger.new[i]);
		}
	}
	
	c_JavaScript.opAPedido(ids);
	
}