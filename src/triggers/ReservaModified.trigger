trigger ReservaModified on ReservaVuelo__c (before update) {

	for(Integer i=0;i<trigger.new.size();i++){
		//si el origen o el destino han cambiado, cambiar el campo modificado a true
		if((trigger.new[i].Origen__c!=trigger.old[i].Origen__c)||
		(trigger.new[i].Destino__c!=trigger.old[i].Destino__c)){
			trigger.new[i].Modificado__c=true;
		}
	}
}