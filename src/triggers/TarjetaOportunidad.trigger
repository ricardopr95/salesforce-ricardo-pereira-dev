trigger TarjetaOportunidad on Tarjeta_credito__c (after insert) {

	Set<string> setIdContatos=new Set<String>();
	User[] users=[Select Id from User where Name='Reaper Beats'];
	
	for(Tarjeta_credito__c tar:trigger.new){
			setIdContatos.add(tar.Contacto__c);
	}
	
	Contact[] c=[Select Id, Premium__c,Name, AccountId, OwnerId from Contact where (Id in:setIdContatos) and Premium__c=true and (OwnerId not in:users)];
	Opportunity op;
	Opportunity[] ops=new List<Opportunity>();
	for(Tarjeta_credito__c tar:trigger.new){
		for(Contact cont:c){
			if(tar.Contacto__c==cont.id){
				//if(cont.OwnerId!=users[0].id){
					//if(cont.Premium__c==true){
						op=new Opportunity();
						op.CloseDate=Date.newInstance(Integer.valueOf(tar.ano_caducidad__c),
							 Integer.valueOf(tar.mes_caducidad__c), 1);
						op.StageName='No iniciado';
						op.AccountId=cont.AccountId;
						op.Name=cont.name;
						ops.add(op);
					//}
				//}
			}
		}	
	}
	
	insert(ops);
	
}