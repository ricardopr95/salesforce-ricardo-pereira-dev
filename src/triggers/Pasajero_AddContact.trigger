/*Este trigger  rellena el campo ‘Contacto’ automáticamente, 
si existe algún contacto en la entidad contact con el nif igual 
al introducido en el campo nif de la entidad ‘Pasajeros’

Además valida que el NIF*/
trigger Pasajero_AddContact on Pasajero__c (before insert, before update) {
	
	CPasajeros.valNif(trigger.new);
	CPasajeros.localizarContacto(trigger.new);
	
	//CPasajeros.localizarContacto(CPasajeros.valNif(trigger.new));  //<--mejor
}