trigger Account on Account (after insert, after update, before insert, before update) {

	Account[] newAcs=trigger.new;
	Account[] oldAcs=trigger.old;
	
	Map<Id,Account> newmap=trigger.newMap;
	Map<Id,Account> oldmap=trigger.oldMap;
	
	//Set<Account> cuentas=new Set<Account>(trigger.new);
	
	if(trigger.isBefore && trigger.isUpdate){
		Set<Id> idCuentas=newmap.keySet();
		Contact[] contactos=[Select Id, FirstName, LastName, Description, AccountId from Contact where Accountid in:idCuentas];
		/*for(Account cuenta:cuentas){
		cuenta.Description='Si, me recorre';
		}*/
		
		Integer cont=0;
		
		for(Contact contact:contactos){
			cont++;
			//System.debug(cont+'Vueltas');
			if(newmap.get(contact.AccountId).AnnualRevenue!=null){
				//System.debug('En la vuelta '+cont+' el annual revenue no es null.');
				contact.Description='Account\'s annual revenue: '+String.valueOf(newmap.get(contact.AccountId).AnnualRevenue);
			}else{
				contact.Description='Account\'s annual revenue: Bancarrota';
			}
		}
		
		update(contactos);
	
		for (Integer i=0;i<newAcs.size();i++){
			if((newAcs[i].Active__c!=oldAcs[i].Active__c)&&(newAcs[i].Active__c!='yes')){
				newAcs[i].Active__c='yes';
			}//if
		}//for
	}//if
}//trigger